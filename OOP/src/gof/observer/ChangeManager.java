package gof.observer;

public class ChangeManager {

	public void Register(Subject s, Observer o) {
		s.Attach(o);
	}
	
	public void Unregister(Subject s, Observer o) {
		s.Detach(o);
	}
	
	public void NotifyAll(Subject s) {
		s.Notify();
	}

}
