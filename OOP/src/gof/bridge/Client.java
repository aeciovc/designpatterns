package gof.bridge;

public class Client {

	public static void main(String[] args) {
		
		Window win = new IconWindow(new XWindowImpl());
		win.DrawRect();
	}

}
