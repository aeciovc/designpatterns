package gof.bridge;

public interface WindowImpl {

	void DeviceRect(int x, int y, int z);
	void DeviceText(char t, int x, int y);
	void DeviceBitmap(char b, int x, int y);

}
