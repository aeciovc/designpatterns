package gof.builder;

public class TextWidgetConverter extends TextConverter{

	public void ConvertCharacter(char v) {
		System.out.println("TextWidgetConverter>ConvertCharacter");
		super.addText(v);
	}
	public void ConvertHTML(String v) {
		System.out.println("TextWidgetConverter>ConvertHTML");
		super.addText(v);
	}
	public void ConvertParagraph(String v) {
		System.out.println("TextWidgetConverter>ConvertParagraph");
		super.addText(v);
	}
}
