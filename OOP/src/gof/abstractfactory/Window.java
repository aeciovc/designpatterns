package gof.abstractfactory;

public interface Window {
	
	public void onClose();
	public void onMax();
	public void onMin();
	public void setVisible(boolean v);
	public void setSize(int x, int y);

}
