package gof.command;

public class Client {

	public static void main(String[] args) {

		final Receiver receiver = new InsertReceiver();

		final Command insert = new InsertCommand(receiver);
		final Command update = new UpdateCommand(receiver);
		
		insert.execute();
		update.execute();

		final Switch mySwitch = new Switch();
		mySwitch.storeAndExecute(insert);
		mySwitch.storeAndExecute(update);
	}

}
