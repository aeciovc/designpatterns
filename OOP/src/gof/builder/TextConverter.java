package gof.builder;

public abstract class TextConverter{
	
	private StringBuffer text;

	public abstract void ConvertCharacter(char v);
	public abstract void ConvertHTML(String v);
	public abstract void ConvertParagraph(String v);

	public void createText() {
		text = new StringBuffer();
	}
	
	protected void addText(String value) {
		text.append(value);
	}
	
	protected void addText(char value) {
		text.append(value);
	}
	
	public String GetText() {
		return this.text.toString();
	}
}