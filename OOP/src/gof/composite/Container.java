package gof.composite;

public interface Container {
	
	public void Add(Component g);

	public void Remove(Component g);

}
