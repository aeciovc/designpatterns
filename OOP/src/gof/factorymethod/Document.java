package gof.factorymethod;

public interface Document {
	
	public void Create();
	public void Open();
	public void Close();
	public void Save();
	public void Revert();

}
