package gof.observer;

public class SystemObserver1 extends Observer{

	@Override
	public void Update(Subject theChangedSubject) {
		System.out.println("System1 notified, price has changed: "+ theChangedSubject.GetState());
	}

}
