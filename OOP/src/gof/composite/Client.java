package gof.composite;

public class Client {
	public static void main(String[] args) {
		
		Component p = new Picture();

		Component t = new Text();
		
		Component l = new Line();
		l.Add(t);
		
		p.Add(t);
		p.Add(l);
		
		p.Draw();
		
		//Case1
		
	}
}
