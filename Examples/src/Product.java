
public class Product {
	
	String name;
	int price;
	
	public Product addName(String name) {
		this.name = name;
		return this;
	}
	
	public Product addPrice(int price) {
		this.price = price;
		return this;
	}
	
	public void finalize(){
		System.out.println(this.name);
	}

}
