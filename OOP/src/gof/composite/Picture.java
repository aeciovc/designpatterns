package gof.composite;

import java.util.ArrayList;

public class Picture extends Component implements Container {

	ArrayList<Component> list = new ArrayList<Component>();
	
	//@Override
	public void Add(Component g) {
		this.list.add(g);
	}

	//@Override
	public void Remove(Component g) {
		this.list.remove(g);
	}
	
	@Override
	public void Draw() {
		// TODO Auto-generated method stub
		System.out.println("Drawing Pictue");
		for (Component graphic : list) {
			graphic.Draw();
		}
	}

}
