package gof.command;

public class InsertCommand implements Command {

	private Receiver receiver;

	public InsertCommand(final Receiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		receiver.beginTransaction();
		receiver.insert();
		receiver.commitTransaction();
	}

}
