package interfaces;

public class DAO {
	
	private SQLiteTransactionListener listener;
	
	public void setListener(SQLiteTransactionListener listener) {
		this.listener = listener;
	}

	public void create() {
		//Begin Transaction
		if (listener != null)
			listener.onBegin();
		
		//Create couse on DB
		
		if (listener != null)
			listener.onCommit();
		//Commit or Roolback
	}

}
