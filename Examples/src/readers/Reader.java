package readers;

import java.io.IOException;

public abstract class Reader {
	
	public void open(String name) {
		System.out.println("Opening...");
	}

	abstract void close() throws IOException;

	abstract int read(char[] arg0, int arg1, int arg2) throws IOException;

}
