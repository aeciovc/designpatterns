package oop.interfaces;

interface DAO {
	
	public Course create(String name);

    public void update(Course curso);

    public void delete(long id);

    public Course findByName(String name);

}