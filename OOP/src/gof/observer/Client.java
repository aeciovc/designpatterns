package gof.observer;

import java.util.Timer;

public class Client {

	public static void main(String[] args) {
		Observer system1 = new SystemObserver1();
		Observer system2 = new SystemObserver2();
		
		Subject subBitcoin = new BitcoinSubject();
		
		ChangeManager manager = new ChangeManager();
		manager.Register(subBitcoin, system1);
		manager.Register(subBitcoin, system2);
		
		
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//Searching prices....
		subBitcoin.SetState("New price found is 1.4536769679");
		
		
		//Notify Observers
		manager.NotifyAll(subBitcoin);

	}

}
