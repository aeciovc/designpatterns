package gof.abstractfactory;

import gof.abstractfactory.motif.MotifWidgetFactory;

public class Client {

	public static void main(String[] args) {

		
		WidgetFactory w = WidgetFactory.CreateWidget(MotifWidgetFactory.class);

		ScrollBar scroll = w.CreateScrollBar();
		scroll.setVisible(true);
		
		Window window = w.CreateWindow();
		window.setSize(30, 40);

	}

}
