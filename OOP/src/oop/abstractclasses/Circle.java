package oop.abstractclasses;

public class Circle extends GraphicObject {
    void draw() {
        //...
    }
    void resize() {
        //...
    }
}