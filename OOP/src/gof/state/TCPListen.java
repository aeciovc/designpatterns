package gof.state;

public class TCPListen extends TCPState{

	@Override
	public void Open(TCPConnection conn) {
		System.out.println("[TCPListen] Connection already Listen");
	}

	@Override
	public void Close(TCPConnection conn) {
		System.out.println("[TCPListen] Connection already Listen");
	}

	@Override
	public void Acknowledge(TCPConnection conn) {
		System.out.println("[TCPListen] Making SYN,ACK...");
		conn.setCurrentState(TCPConnection.TCPEstablished);
		
	}

}
