package models;

public class File {
	private String name;
	private String path;
	private String contentType;
	private float lenght;
	
	public File() {
		
	}
	
	public File(String path, String name) {
		this.path = path;
		this.name = name;
	}
	
	public File(String ct) {
		this.contentType = ct;
	}

	boolean exists() {
		return false;
	}

	void create() {
	}

	void remove() {
	}
	
	public String getFullName() {
		return this.path + "/" + this.name;
	}
	
	public String getContentType() {
		return this.contentType;
	}

	public float getLenght() {
		return lenght;
	}

	public void setLenght(float lenght) {
		this.lenght = lenght;
	}
}


