package gof.bridge;

public class IconWindow extends Window {

	WindowImpl impl;
	
	public IconWindow(WindowImpl impl) {
		super(impl);
		this.impl = impl;
	}
	
	@Override
	void DrawRect() {
		
		System.out.println("Drawing on Icon Bitmap");
		
		this.impl.DeviceBitmap('L', 100, 100);
		
	}

	@Override
	void DrawText() {
		
	}
}
