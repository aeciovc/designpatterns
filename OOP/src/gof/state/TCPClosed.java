package gof.state;

public class TCPClosed extends TCPState {

	@Override
	public void Open(TCPConnection conn) {
		System.out.println("[TCPClosed] Requesting a new connection...");
		conn.setCurrentState(TCPConnection.TCPListen);
	}

	@Override
	public void Close(TCPConnection conn) {
		System.out.println("[TCPClosed] Connection already Closed");
	}

	@Override
	public void Acknowledge(TCPConnection conn) {
		System.out.println("[TCPClosed] Connection needs is Litening to SYN and ACK");
	}

}
