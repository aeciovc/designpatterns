package gof.factorymethod;

public abstract class Application {
	
	private Document DefaultDocument;
	
	public Document FactoryMethod() {
		return DefaultDocument;
	}
	
	public abstract Document CreateDocument();
	public abstract Document OpenDocument(String path);
	
	public Document NewDocument() {
		return CreateDocument();
	}
	
	public Document Open(String path) {
		return OpenDocument(path);
	}

}
