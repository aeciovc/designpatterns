package oop.Inheritance;

public class Activity {
    
	protected void onCreate(Bundle savedInstanceState) {
		
	}

    protected void onStart() {
    	
    }

    protected void onRestart() {
    	
    }

    protected void onResume() {
    	
    }

    protected void onPause() {
    	
    }

    protected void onStop() {
    	
    }

    protected void onDestroy() {
    	
    }
    
    void setContentView (int view) {
    	
    }
}
