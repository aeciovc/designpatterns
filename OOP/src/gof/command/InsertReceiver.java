package gof.command;

public class InsertReceiver implements Receiver{

	@Override
	public void insert() {
		System.out.println("INSERT INTO...");
	}

	@Override
	public void update() {
		System.out.println("UPDATE <TABLE> SET...");
	}

	@Override
	public void beginTransaction() {
		System.out.println("BEGIN TRANSACTION...");
	}

	@Override
	public void commitTransaction() {
		System.out.println("COMMIT TRANSACTION...");
	}

	@Override
	public void delete() {
		System.out.println("DELETE FROM...");
	}

}
