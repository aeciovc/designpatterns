package gof.abstractfactory;

import gof.abstractfactory.motif.MotifWidgetFactory;
import gof.abstractfactory.pm.PMWidgetFactory;

public abstract class WidgetFactory {
	
	public abstract ScrollBar CreateScrollBar();
	public abstract Window CreateWindow();
	
	public static WidgetFactory CreateWidget(Class<?> c) {
		if(c.isInstance( new MotifWidgetFactory())) {
			return new MotifWidgetFactory();
		}else if (c.isInstance(new PMWidgetFactory())) {
			return new PMWidgetFactory();
		}
		return null;
	}
	

}
