package gof.command;

/** The Command interface */
public interface Command {
	void execute();
}
