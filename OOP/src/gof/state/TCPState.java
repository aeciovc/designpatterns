package gof.state;

public abstract class TCPState {
	
	public abstract void Open(TCPConnection conn);
	public abstract void Close(TCPConnection conn);
	public abstract void Acknowledge(TCPConnection conn);
}
