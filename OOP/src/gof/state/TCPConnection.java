package gof.state;

public class TCPConnection {

	private TCPState currentState;
	
	static TCPState TCPListen = new TCPListen();
	static TCPState TCPEstablished = new TCPEstablished();
	static TCPState TCPClosed = new TCPClosed();
	
	public TCPConnection(){
		this.currentState = TCPClosed;
	}

	public void Open() {
		currentState.Open(this);
	}

	public void Close() {
		currentState.Close(this);
	}

	public void Acknowledge() {
		currentState.Acknowledge(this);
	}

	public TCPState getCurrentState() {
		return currentState;
	}

	public void setCurrentState(TCPState currentState) {
		this.currentState = currentState;
	}

}
