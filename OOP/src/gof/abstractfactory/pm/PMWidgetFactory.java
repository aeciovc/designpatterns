package gof.abstractfactory.pm;

import gof.abstractfactory.ScrollBar;
import gof.abstractfactory.WidgetFactory;
import gof.abstractfactory.Window;

public class PMWidgetFactory extends WidgetFactory {
	
	PMScrollBar scroll;
	PMWindow window;

	@Override
	public ScrollBar CreateScrollBar() {
		return this.scroll;
	}

	@Override
	public Window CreateWindow() {
		return this.window;
	}

}
