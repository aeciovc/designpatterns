package gof.state;

public class Client {

	public static void main(String[] args) {
		
		TCPConnection conn = new TCPConnection();
		conn.Open();
		
		System.out.println("State after Open():"+ conn.getCurrentState());
		conn.Close();
		
		System.out.println("State after Close():"+ conn.getCurrentState());
		

	}

}
