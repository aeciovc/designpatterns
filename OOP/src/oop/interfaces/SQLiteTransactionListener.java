package oop.interfaces;

public interface SQLiteTransactionListener {
	void onBegin();
	void onCommit();
	void onRollback();
}




