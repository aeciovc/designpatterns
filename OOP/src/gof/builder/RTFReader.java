package gof.builder;

public class RTFReader {
	
 	private TextConverter converter;

 	RTFReader(TextConverter c) {
 		this.converter = c;
 	}

 	public void readRTF(String text) {
 		//Initialize the text
 		converter.createText();
 		
 		//Iterate to discovery char and html
 		converter.ConvertCharacter('A');
 		converter.ConvertHTML("<html...>");
 		converter.ConvertParagraph("   Paragraph");
 	}
 	
 	public String getFinalText() {
 		return converter.GetText();
 	}

}
