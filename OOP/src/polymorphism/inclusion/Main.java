package polymorphism.inclusion;

import java.math.BigInteger;

public class Main {

	public static void main(String[] args) {
		
		Number v1 = BigInteger.valueOf(50);
		
		Number v2 = new Integer(2);
		
		System.out.println("Max number is "+ max(v1, v2));
		
		
		System.out.println("Hello World");
		System.out.println(1);
		System.out.println(true);
		
		
		
		MailSender mailSender = new JavaMailSenderImpl(); //Injection Dependency;
		
		SimpleMailMessage message = new SimpleMailMessage();

		message.setFrom("aeciovc@gmail.com");
		message.setTo("contato@aecio.com");
		message.setSubject("Enviando Email");
		message.setText("Segue o email");
		mailSender.send(message);
		
		
	}
	
	static Number max(Number n1, Number n2){
		if (n1.intValue() > n2.intValue()) {
			return n1;
		}else {
			return n2;
		}
	}

}

