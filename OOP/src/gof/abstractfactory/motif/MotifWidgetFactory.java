package gof.abstractfactory.motif;

import gof.abstractfactory.ScrollBar;
import gof.abstractfactory.WidgetFactory;
import gof.abstractfactory.Window;

public class MotifWidgetFactory extends WidgetFactory {

	MotifScrollBar scroll;
	MotifWindow window;
	
	@Override
	public ScrollBar CreateScrollBar() {
		return this.scroll;
	}

	@Override
	public Window CreateWindow() {
		return this.window;
	}

}