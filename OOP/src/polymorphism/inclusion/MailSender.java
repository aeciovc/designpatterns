package polymorphism.inclusion;

public interface MailSender {
	
	void send(SimpleMailMessage simpleMessage);
	void send(SimpleMailMessage... simpleMessages);

}
