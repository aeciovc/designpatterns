package gof.command;

/** The Receiver class */
interface Receiver {

	public void beginTransaction();
	public void commitTransaction();
	
	public void insert();

	public void update();

	public void delete();
}
