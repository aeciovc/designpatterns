package gof.bridge;

public abstract class Window {

	WindowImpl impl;
	
	abstract void DrawText();

	abstract void DrawRect();

	public Window(WindowImpl w) {
		this.impl = w;
	}

}
