package oop.abstractclasses;

public class Rectangle extends GraphicObject {
    void draw() {
        //...
    }
    void resize() {
        //...
    }
}
