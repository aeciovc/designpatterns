package gof.command;

public class UpdateCommand implements Command{
	
	private Receiver receiver;

	public UpdateCommand(final Receiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void execute() {
		receiver.update();
	}

}
