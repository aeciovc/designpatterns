package exercise;

public class Session {

	TransactionListener listener;
	
	public void SetListener(TransactionListener tranListener) {
		this.listener = tranListener;
	}

	public void close() {
	}

	public ITransaction beginTransaction() {
		this.listener.onBegin();
		return null;
	}
}
