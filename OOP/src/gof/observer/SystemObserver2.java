package gof.observer;

public class SystemObserver2 extends Observer{

	@Override
	public void Update(Subject theChangedSubject) {
		System.out.println("System2 notified: Bitcoin has changed its price to: "+ theChangedSubject.GetState());
	}

}
