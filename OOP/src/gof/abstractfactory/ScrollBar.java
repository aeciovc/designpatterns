package gof.abstractfactory;

public interface ScrollBar {
	
	public void onScroll(int position);
	public void setVisible(boolean v);

}
