package exercise;

public abstract class TransactionListener {

	abstract void onBegin();

	abstract void onCommit();

	abstract void onRollback();
}
