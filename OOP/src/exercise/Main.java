package exercise;

public class Main {

	public static void main(String[] args) {

		TransactionListener tranListener = new TransactionListener() {

			@Override
			void onBegin() {
				System.out.println("onBegin executed");
			}

			@Override
			void onCommit() {
				System.out.println("onCommit executed");
			}

			@Override
			void onRollback() {
				System.out.println("onRollback executed");
			}

		};

		Session sess = new Session();//factory.openSession();
		
		ITransaction tx = null;
		sess.SetListener(tranListener);
		try {
			tx = sess.beginTransaction();
			// do some work
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			sess.close();
		}

	}

}
