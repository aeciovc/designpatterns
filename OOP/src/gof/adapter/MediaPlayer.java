package gof.adapter;

interface MediaPlayer {
	void play(String filename);
}
