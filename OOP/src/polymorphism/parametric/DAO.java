package polymorphism.parametric;

import java.util.ArrayList;
import java.util.List;

public class DAO<T> {
	
	public T save(T entity) {
		//save on database
		return entity;
	}
	
	public T delete(T entity) {
		//delete from database
		return entity;
	}
	
	public T update(T entity) {
		//save on database
		return entity;
	}
	
	public List<T> findByName(String name) {
		//get Entity on database
		return new ArrayList<T>();
	}

}
