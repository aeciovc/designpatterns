package gof.builder;

public class Client {

	public static void main(String[] args) {

		
        TextConverter conv1 = new ASCIIConverter();
        TextConverter conv2 = new TeXConverter();

        RTFReader reader = new RTFReader(conv1);
        reader.readRTF("Long text here....");

        String text = reader.getFinalText();
        System.out.println("Text Final is: "+ text);

	}

}
