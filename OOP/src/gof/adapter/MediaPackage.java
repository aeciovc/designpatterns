package gof.adapter;

public interface MediaPackage {
	void playFile(String filename);
}
