package gof.bridge.jdbc;

public interface Connection {
	
	public Statement createStatement(); 

}
