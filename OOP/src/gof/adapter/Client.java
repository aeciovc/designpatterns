package gof.adapter;

public class Client {

	public static void main(String[] args) {
		MediaPlayer player = new MP3();
		player.play("file.mp3");
		
		MediaPlayer player2 = new FormatAdapter(new MP4());
		player2.play("file.mp4");
		
		MediaPlayer player3 = new FormatAdapter(new VLC());
		player3.play("file.avi");
	}

}
