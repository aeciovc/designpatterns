package printwritter;

public class PrintWriter {
	
	private String name;
	private String encode;
	private StringBuffer content;
	
	public PrintWriter(String name, String encode) {
		this.name = name;
		this.encode = encode;
		this.content = new StringBuffer();
	}
	
	public void println(String content) {
		this.content.append(content);
	}
	
	public void close() {
		
	}
	
	public void printAll() {
		System.out.println("Name:"+ this.name);
		System.out.println("Encode:"+ this.encode);
		System.out.println("Content:"+ this.content);
	}

}
