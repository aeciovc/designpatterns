package gof.builder;

public class ASCIIConverter extends TextConverter{
	
	public void ConvertCharacter(char v) {
		System.out.println("ASCIIConverter>ConvertCharacter");
		super.addText(v);
	}
	public void ConvertHTML(String v) {
		System.out.println("ASCIIConverter>ConvertHTML");
		super.addText(v);
	}
	public void ConvertParagraph(String v) {
		System.out.println("ASCIIConverter>ConvertParagraph");
		super.addText(v);
	}
}
