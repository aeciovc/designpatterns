package gof.bridge.jdbc;

public class Cliente {
	
	public static void main(String[] args) {
		
		
		Connection con =
				  DriverManager.getConnection("jdbc:odbc:somedb", "user", "passwd");
		
		Statement stmt = con.createStatement();
		
		//ResultSet rs = stmt.executeQuery("SELECT * FROM CUSTOMERS");
	}

}
