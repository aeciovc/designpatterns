package gof.observer;

public class BitcoinSubject extends Subject {
	
	private String state;

	@Override
	public String GetState() {
		return state;
	}

	@Override
	public void SetState(String s) {
		this.state = s;
	}

}
