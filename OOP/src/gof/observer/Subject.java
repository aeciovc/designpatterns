package gof.observer;

import java.util.ArrayList;
import java.util.List;

public abstract class Subject {
	
	private List<Observer> observers = new ArrayList<Observer>();
	
	public abstract String GetState();
	public abstract void SetState(String s);
	
	public void Attach(Observer o) {
		observers.add(o);
	}
	
	public void Detach(Observer o) {
		observers.remove(o);
	}
	
	public void Notify() {
		for (Observer observer : observers) {
			observer.Update(this);
		}
	}
}
