package polymorphism.parametric;

import java.util.ArrayList;
import java.util.List;

public class MyList<T> {
	private List<T> itemList = new ArrayList<T>();

	public void add(T item) {
		itemList.add(item);
	}

	public T get(int index) {
		return itemList.get(index);
	}

	public static void main(String args[]) {
		
		MyList<String> listStr = new MyList<String>();
		listStr.add("Lion");
		String str = listStr.get(0);
		System.out.println(str);

		MyList<Integer> listInt = new MyList<Integer>();
		listInt.add(new Integer(100));
		Integer integerObj = listInt.get(0);
		System.out.println(integerObj);
	}
	
	
}
