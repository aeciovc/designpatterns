package gof.bridge;

public class XWindowImpl implements WindowImpl {

	@Override
	public void DeviceRect(int x, int y, int z) {
		
	}

	@Override
	public void DeviceText(char t, int x, int y) {
		
	}

	@Override
	public void DeviceBitmap(char b, int x, int y) {
		System.out.println("DeviceBitmap on XWindow");
	}

}
