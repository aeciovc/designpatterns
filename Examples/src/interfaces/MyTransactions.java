package interfaces;

public class MyTransactions implements SQLiteTransactionListener {

	@Override
	public void onBegin() {
		System.out.println("onBegin");
	}

	@Override
	public void onCommit() {
		System.out.println("onCommit");
	}

	@Override
	public void onRollback() {
		System.out.println("OnRollback");
	}

}