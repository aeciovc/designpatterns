package gof.factorymethod;

public class MyDocument implements Document {

	@Override
	public void Create() {
		System.out.println("Creating MyDocument");
	}

	@Override
	public void Open() {
		System.out.println("Opening MyDocument");
	}

	@Override
	public void Close() {
		System.out.println("Closing MyDocument");
	}

	@Override
	public void Save() {
		System.out.println("Saving MyDocument");
	}

	@Override
	public void Revert() {
		System.out.println("Reverting MyDocument");
	}

}
