package exercise.activity;

public abstract class OnClickListener {
	
	public abstract void onClick(View v);

}
