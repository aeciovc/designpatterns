package gof.facade;

public class Client {
	
	public static void main(String[] args) {
		
		OrderServiceFacade facade = new OrderServiceFacadeImpl();
		boolean result = facade.placeOrder(10);
		System.out.println("Result order: "+result);
		
	}

}
