package gof.factorymethod;

public class MyApplication extends Application{

	@Override
	public Document CreateDocument() {
		System.out.println("Creating Document MyApplication");
		Document doc = new MyDocument();
		doc.Create();
		return doc;
	}
	
	@Override
	public Document OpenDocument(String path) {
		System.out.println("Opening Document MyApplication");
		Document doc = new MyDocument();
		doc.Open();
		return doc;
	}

}
