package gof.iterator;

public class Client {

	public static void main(String[] args) {
		MyList myList = new MyListImpl();

		for (Iterator iter = myList.getIterator(); iter.hasNext();) {
			String name = (String) iter.next();
			System.out.println("Name : " + name);
		}

	}

}
