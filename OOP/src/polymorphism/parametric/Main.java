package polymorphism.parametric;

import java.util.List;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		User u = new User();
		
		DAO<User> dao = new DAO<User>();
		dao.save(u);

		List<User> users = dao.findByName("joao");
		
		dao.delete(users.get(0));
	}

}
