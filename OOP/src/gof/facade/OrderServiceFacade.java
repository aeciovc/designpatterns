package gof.facade;

public interface OrderServiceFacade {
	boolean placeOrder(int productId);
}
