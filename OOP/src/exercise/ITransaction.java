package exercise;

public interface ITransaction {
	
	abstract void begin();

	abstract void commit();

	abstract void rollback();

}
