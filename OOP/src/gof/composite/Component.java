package gof.composite;

public abstract class Component {

	public abstract void Draw();

	public abstract void Add(Component g);
	public abstract void Remove(Component g);

	public Component GetChild(int i) {
		return null;
	}

}
