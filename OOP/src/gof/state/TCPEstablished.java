package gof.state;

public class TCPEstablished extends TCPState{

	@Override
	public void Open(TCPConnection conn) {
		System.out.println("Connection already established");
	}

	@Override
	public void Close(TCPConnection conn) {
		System.out.println("[TCPEstablished] Closing connection...");
		conn.setCurrentState(TCPConnection.TCPClosed);
	}

	@Override
	public void Acknowledge(TCPConnection conn) {
		System.out.println("Connection already SYN and ACK");
	}

}
